	****************    ThemeCloud     ***********************

L�applicazione riguarda una piattaforma di condivisione file per chiunque voglia che permetti di caricare, rimuovere, ricercare ed eventualmente commentare e/o votare un file di qualunque tipo, documenti testuali, immagini, video ecc.

L idea di base � che possa essere utilizzata da hobbisti o/e appassionati di varie attivit� come riferimento informativo e consultativo per i propri interessi comuni.

L�accesso alla piattaforma � possibile tramite il proprio indirizzo e-mail, durante la registrazione agli utenti vengono chiesti i propri interessi che saranno poi utilizzati per presentare dinamicamente i contenuti di maggiore gradimento nella homepage.
In alternativa � possibile effettuare l�accesso sfruttando l�integrazione con i social network dal quale l�apprendimento dei propri interessi potr� essere fatto in maniera automatica sfruttando le preferenze del proprio account.

Ad ogni utilizzo dell�applicazione viene effettuata in automatico una profilazione dell�utente in base alle ricerche effettuate dall�utente stesso che contribuiranno poi alla successiva generazione dinamica della home.
 
La ricerca dei contenuti avviene tramite nome file o specificazione dei tag. Ad ogni file caricato sulla piattaforma vengono associati dei tag di classificazione che ne faciliteranno poi la ricerca.

Sia i risultati di una ricerca che la presentazione dinamica nella home � basata sul numero di visualizzazioni e sul gradimento che i file hanno ottenuto.
Un utente pu� commentare un file inserendo soltanto del testo e/o aggiungere un gradimento (+/-). Ogni volta che un file viene aperto e/o commentato viene aggiornato il numero di visualizzazioni e dei commenti, se necessario.



	*** Tecnologie utilizzate ***

La piattaforma si comporr� di un server web Apache sul quale � installato un CMS (Drupal) per sfruttarne anche il database.
Per le esigenze di storage verr� utilizzata la doppia soluzione con  database Drupal e un servizio Dropbox.

Il servizio Dropbox conterr� i file veri � propri caricati sulla piattaforma.
Mentre nel database Drupal verranno memorizzati i dati relativi al funzionamento della logica applicativa.


	*** Struttura base di dati ***

- Di ogni profilo utente interessano: l�username(univoco), la password  ed eventuali dati anagrafici. Ogni utente inserisce un elenco dei suoi interessi iniziali.
- Di ogni ricerca effettuata interessa: l�utente, il tag ricercato, il numero di volte che � stato cercato, e la data di ultima ricerca.


	## Soluzione Francesco ##

- Di un file interessano il nome, chi lo ha inserito (creatore), la data di inserimento, il link (URL al filesystem locale o esterno), i tag, il numero di visualizzazioni.
- Per ogni file � possibile inserire un commento di cui interessa: il file a cui si riferisce, l�username che lo inserisce, il commento vero e proprio e il timestamps.
- Un utente pu� inserire un solo gradimento per ogni file di cui interessa: il file, l�username (+ o -).

	## Soluzione Daniele ##
- Di ogni file interessano: nome, user che lo inserisce, tag, link, timestamps.
- Della valutazione interessano: il file, n� di +, n� di -, n� di commenti e n� di visualizzazioni.
